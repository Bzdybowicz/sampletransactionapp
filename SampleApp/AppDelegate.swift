//
//  AppDelegate.swift
//  SampleApp
//
//  Created by Zdybowicz Bartlomiej on 29/03/2017.
//  Copyright © 2017 BZ. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}

