//
//  ViewController.swift
//  SampleApp
//
//  Created by Zdybowicz Bartlomiej on 29/03/2017.
//  Copyright © 2017 BZ. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var transactionDataSourceDelegate: TransactionDataSourceDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let transactionModel = TransactionCellModel(name:"testName",
                                                    receiver:"Receiver",
                                                    amountText:"453,44 USD")
        transactionDataSourceDelegate.transactionViewModel = TransactionViewModel(transactions: [transactionModel])
    }
}
