//
//  TransactionViewModel.swift
//  SampleApp
//
//  Created by Zdybowicz Bartlomiej on 29/03/2017.
//  Copyright © 2017 BZ. All rights reserved.
//

import Foundation

protocol TransactionViewModelProtocol {
    var transactions: [TransactionCellModelProtocol] {get set}
}

struct TransactionViewModel {
    var transactions: [TransactionCellModelProtocol]
}
