//
//  TransactionViewModel.swift
//  SampleApp
//
//  Created by Zdybowicz Bartlomiej on 29/03/2017.
//  Copyright © 2017 BZ. All rights reserved.
//

import Foundation

protocol TransactionCellModelProtocol {
    var name: String {get set}
    var receiver: String {get set}
    var amountText: String {get set}
}

struct TransactionCellModel: TransactionCellModelProtocol {
    
    var name: String
    var receiver: String
    var amountText: String
}
