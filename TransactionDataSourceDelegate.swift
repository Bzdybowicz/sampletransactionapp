//
//  TransactionDataSourceDelegate.swift
//  SampleApp
//
//  Created by Zdybowicz Bartlomiej on 29/03/2017.
//  Copyright © 2017 BZ. All rights reserved.
//

import UIKit

class TransactionDataSourceDelegate: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var transactionViewModel: TransactionViewModel?
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionCell", for: indexPath)
        
        let model = transactionViewModel?.transactions[indexPath.row]
        cell.textLabel?.text = model?.name
        cell.detailTextLabel?.text = model?.receiver
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactionViewModel?.transactions.count ?? 0
    }
}
